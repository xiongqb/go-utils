# GO 推荐VSCode  （idea2023.2.5支持不够理想）
### 安装
    # 配置 GOPROXY 环境变量，以下三选一

    # 1. 七牛 CDN
    go env -w  GOPROXY=https://goproxy.cn,direct
    
    # 2. 阿里云(目前不能用)
    go env -w GOPROXY=https://mirrors.aliyun.com/goproxy/,direct
    
    # 3. 官方
    go env -w  GOPROXY=https://proxy.golang.org,direct
    go env -w  GOPROXY=https://goproxy.io,direct

### 初始化项目
    go mod init 模块名 //为项目指定模块名使其模块化（创建go.mod）
    不要轻易手动删除 go.sum 文件。
    如果需要更新依赖，应使用 go get -u 或 go mod tidy 等命令来操作。
    可以执行 go mod tidy 或 go mod verify 命令来修复或更新 go.sum 文件
    如果出现依赖问题，可执行：go clean -modcache 清除后再执行：go mod tidy