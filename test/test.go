package main

import (
	"fmt"
	"os/exec"

	"gitee.com/xiongqb/go-utils/common"
)

func main() {
	fmt.Println("测试开始")
	//reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths" /f "*browser.exe" /s
	//reg query 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths' /f "*browser.exe" /s
	testFileOf()
	// tst()
	fmt.Println("测试结束")
}
func testFileOf() {
	fmt.Println("---->", common.FileOf("/opt/class/b", "./c", "/a/b/", "/.mp3"))
}
func testCmd() {
	// text := common.CmdRun("cmd", "/K", "reg", "query", `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths`)
	// text := common.CmdRun("cmd", "/C", "ipconfig")
	// text := common.CmdRun("PowerShell.exe", "reg", "query", `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths`)
	// text := common.CmdRun("PowerShell", "reg", "query", `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths`)
	text := common.CmdRun("PowerShell", "reg", "query", `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths`, "/f", "*edge.exe", "/s")
	fmt.Println("---->", text)
}
func testStartWinApp() {
	err := common.StartWinApp(false, "chrome1", "http://www.baidu.com")
	if err != nil {
		fmt.Println("err", err.Error())
		// 检查错误是否是ExitError类型
		if exitError, ok := err.(*exec.ExitError); ok {
			// 获取命令的退出状态码
			exitCode := exitError.ExitCode()
			fmt.Printf("命令执行失败，退出状态码: %d\n", exitCode)
			// 根据退出状态码进行判断处理
			if exitCode == 1 {
				fmt.Println("命令返回特定的错误码")
			} else {
				fmt.Println("命令返回未知错误")
			}
		} else {
			// 不是ExitError，则可能是其他类型的错误，如执行命令前的错误（如文件找不到）
			fmt.Println("命令执行过程中的非退出错误:", err)
		}
	} else {

		fmt.Println("没有错误")
	}
}
func testFile() {
	fmt.Println(common.FileOf("/home/x", "..\\a", ".x"))
}
