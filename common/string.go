package common

import (
	"container/list"
	"fmt"
	"log"
	"math/rand"
	"net/url"
	"reflect"
	"regexp"
	"strings"
	"time"
	"unicode/utf8"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

// 根据长度生成对应长度的随机字符串
func StrRandom(length int) string {
	// 设置随机种子
	rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

// StrJoin 接受任意数量的字符参数，并用指定分隔符连接
func StrJoin(separator string, strs ...string) string {
	return strings.Join(strs, separator)
}

// StrJoinObj 接受任意数量的interface{}参数，尝试将它们转换为字符串并用指定分隔符连接
func StrJoinObj(separator string, objs ...interface{}) string {
	var strSlice []string
	for _, item := range objs {
		// 使用 fmt.Sprint 将任意类型转换为字符串
		strItem := fmt.Sprint(item)
		strSlice = append(strSlice, strItem)
	}
	// 使用 strings.Join 连接字符串切片
	return strings.Join(strSlice, separator)
}
func StrToUpperByFirstLetter(s string) string {
	if s == "" {
		return ""
	}
	// 获取字符串的第一个 rune
	firstRune, size := utf8.DecodeRuneInString(s)
	// 将第一个 rune 转换成大写
	uppercaseFirst := strings.ToUpper(string(firstRune))
	// 结合剩余部分的字符串
	tail := s[size:]
	return uppercaseFirst + tail
}
func StrUrlEncode(plainText string) string {
	return url.QueryEscape(plainText)
}
func StrUrlDecode(encodedStr string) string {
	decodedStr, err := url.QueryUnescape(encodedStr)
	if err != nil {
		log.Println("Error:", err)
		return ""
	} else {
		fmt.Println(decodedStr)
		return decodedStr
	}
}
func StrEqualIgnoreCase(str1 string, str2 string) bool {
	return strings.EqualFold(str1, str2)
}
func StrStartsWith(str string, starts string) bool {
	return strings.HasPrefix(str, starts)
}
func StrEndsWith(str string, ends string) bool {
	return strings.HasSuffix(str, ends)
}
func StrTrim(str string) string {
	return strings.TrimSpace(str)
}
func StrIsEmpty(obj any) bool {
	if obj == nil {
		return true
	}
	log.Println(reflect.TypeOf(obj))
	//fmt.Println(reflect.TypeOf(obj) == reflect.TypeOf("string"))
	if IsInstanceOf(obj, "string") {
		return len(strings.TrimSpace(fmt.Sprintf("%s", obj))) == 0
	}
	return false
}
func StrIsNotEmpty(obj any) bool {
	return !StrIsEmpty(obj)
}

func StrSubstringBegin(str string, begin int) string {
	return str[begin:]
}
func StrSubstringEnd(str string, end int) string {
	return str[:end]
}
func StrSubstring(str string, begin int, end int) string {
	return str[begin:end]
}

// StrSplit(str,"@") 根据分割符号separator拆分成数组（不包含分割符号separator）
func StrSplit(str, separator string) []string {
	split := strings.Split(str, separator)
	size := len(split)
	lst := list.New()
	j := 0
	for i := 0; i < size; i++ {
		s := split[i]
		if s == separator {
			continue
		}
		if len(s) > 0 {
			lst.PushBack(s)
			j++
		}
	}
	return ListToArrayStr(lst)
}

// StrSplitByRegexp(str,"@#*") 根据分割符号【集】separators拆分成数组（不包含分割符号集separators）
func StrSplitByRegexp(str string, separators string) []string {
	re := regexp.MustCompile("[" + separators + "]")
	parts := re.Split(str, -1)
	return removeEmptyEntries(parts)
}

func removeEmptyEntries(strs []string) []string {
	result := []string{}
	for _, s := range strs {
		if s != "" {
			result = append(result, s)
		}
	}
	return result
}
