package common

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

// 设置超时时间 注意：超时包括连接时间、任何重定向和读取响应正文。
// 设置为0，则不设置
var HttpTimeout = 0

type HeaderPair struct {
	Key   string
	Value string
}

func HttpGet(url string, headers ...string) (string, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return "", err
	}
	// 将不定数量的 headers 转换为键值对
	for i := 0; i < len(headers)-1; i += 2 {
		if i+1 >= len(headers) {
			return "", fmt.Errorf("headers 应以键值对的形式提供")
		}
		req.Header.Set(headers[i], headers[i+1])
	}
	//重新创建客户端，以便设置header
	var client *http.Client
	if HttpTimeout > 0 {
		log.Println("设置全局 HttpTimeout:", HttpTimeout, "秒")
		client = &http.Client{
			Timeout: time.Duration(HttpTimeout) * time.Second,
		}
	} else {
		client = &http.Client{}
	}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	bytes_, _ := io.ReadAll(resp.Body)
	return string(bytes_), nil
}
func HttpGetToBytes(url string, headers ...string) ([]byte, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	// 将不定数量的 headers 转换为键值对
	for i := 0; i < len(headers)-1; i += 2 {
		if i+1 >= len(headers) {
			return nil, fmt.Errorf("headers 应以键值对的形式提供")
		}
		req.Header.Set(headers[i], headers[i+1])
	}
	//重新创建客户端，以便设置header
	var client *http.Client
	if HttpTimeout > 0 {
		log.Println("设置全局 HttpTimeout:", HttpTimeout, "秒")
		client = &http.Client{
			Timeout: time.Duration(HttpTimeout) * time.Second,
		}
	} else {
		client = &http.Client{}
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	bytes_, _ := io.ReadAll(resp.Body)
	//defer resp.Body.Close()
	return bytes_, nil
}
func HttpDownload(url string, file string, headers ...string) error {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}
	// 将不定数量的 headers 转换为键值对
	for i := 0; i < len(headers)-1; i += 2 {
		if i+1 >= len(headers) {
			log.Println("headers 应以键值对的形式提供")
			return fmt.Errorf("headers 应以键值对的形式提供")
		}
		req.Header.Set(headers[i], headers[i+1])
	}
	//重新创建客户端，以便设置header
	var client *http.Client
	if HttpTimeout > 0 {
		log.Println("设置全局 HttpTimeout:", HttpTimeout, "秒")
		client = &http.Client{
			Timeout: time.Duration(HttpTimeout) * time.Second,
		}
	} else {
		client = &http.Client{}
	}
	resp, err := client.Do(req)
	if err != nil {
		//ThrowException("HttpDownload file [", url, "]", err.Error())
		return err
	}
	defer resp.Body.Close()
	if !FileDirsReadyForWriteFile(file) {
		log.Println("download.FileDirsReadyForWriteFile failed")
		return fmt.Errorf("download.FileDirsReadyForWriteFile failed")
	}
	bytes_, _ := io.ReadAll(resp.Body)
	err = os.WriteFile(file, bytes_, 0666)
	return err

}
func IsUrl(url string) bool {
	return StrStartsWith(url, "http://") || StrStartsWith(url, "https://")
}
func HttpPost(url string, data []byte, headers ...string) (string, error) {
	// 创建 HTTP 请求
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(data))
	if err != nil {
		return "", err
	}
	// 设置 Content-Type 为 application/json
	req.Header.Set("Content-Type", "application/json")

	// 将不定数量的 headers 转换为键值对
	for i := 0; i < len(headers)-1; i += 2 {
		if i+1 >= len(headers) {
			return "", fmt.Errorf("headers 应以键值对的形式提供")
		}
		req.Header.Set(headers[i], headers[i+1])
	}

	// 发送请求
	var client *http.Client
	if HttpTimeout > 0 {
		log.Println("设置全局 HttpTimeout:", HttpTimeout, "秒")
		client = &http.Client{
			Timeout: time.Duration(HttpTimeout) * time.Second,
		}
	} else {
		client = &http.Client{}
	}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// 可以在这里读取响应 body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}

func HttpPostJSON(url string, jsonData interface{}, headers ...string) (JSONObject, error) {
	// func Post(url string, jsonData any, headers map[string]string)
	// 将数据编码为 JSON 字符串
	jsonBytes, err := json.Marshal(jsonData)
	if err != nil {
		return nil, err
	}
	// 创建 HTTP 请求
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonBytes))
	if err != nil {
		return nil, err
	}
	// 设置 Content-Type 为 application/json
	req.Header.Set("Content-Type", "application/json")

	// 将不定数量的 headers 转换为键值对
	for i := 0; i < len(headers)-1; i += 2 {
		if i+1 >= len(headers) {
			return nil, fmt.Errorf("headers 应以键值对的形式提供")
		}
		req.Header.Set(headers[i], headers[i+1])
	}

	// 发送请求
	var client *http.Client
	if HttpTimeout > 0 {
		log.Println("设置全局 HttpTimeout:", HttpTimeout, "秒")
		client = &http.Client{
			Timeout: time.Duration(HttpTimeout) * time.Second,
		}
	} else {
		client = &http.Client{}
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// 可以在这里读取响应 body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var responseJSON JSONObject
	err = json.Unmarshal(body, &responseJSON)
	if err != nil {
		return nil, err
	}

	return responseJSON, nil
}
