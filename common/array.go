package common

import "fmt"

func ArrayNewStr(array ...string) []string {
	return array
}
func ArrayNewInt(array ...int) []int {
	return array
}

// ArrayNewArrayStr 变长数组，多次重复使用的话有点消耗性能
func ArrayNewArrayStr(arr []string, values ...string) []string {
	size := len(arr)
	size2 := len(values)
	newSize := size + size2
	tmp := make([]string, newSize)
	copy(tmp, arr)
	for i := 0; i < size2; i++ {
		tmp[size+i] = values[i]
	}
	return tmp
}

// ArrayNewArrayInt 变长数组，多次重复使用的话有点消耗性能
func ArrayNewArrayInt(arr []int, values ...int) []int {
	size := len(arr)
	size2 := len(values)
	newSize := size + size2
	tmp := make([]int, newSize)
	copy(tmp, arr)
	for i := 0; i < size2; i++ {
		tmp[size+i] = values[i]
	}
	return tmp
}

// ArrayInsertArrayStr 向数组arr前排插入元素。
// 如果向后最近请直接使用 append(slice []Type, elems ...Type)
func ArrayInsertArrayStr(arr []string, els ...string) []string {
	size := len(els)
	size1 := len(arr)
	newSize := size + size1
	tmp := make([]string, newSize)
	for i := 0; i < newSize; i++ {
		if (i + 1) <= size {
			tmp[i] = els[i]
		} else {
			tmp[i] = arr[i-size]
		}
	}
	return tmp
}

// ArrayRemoveArrayStr 变长数组，多次重复使用的话有点消耗性能
func ArrayRemoveArrayStr(arr []string, removeIndex int) []string {
	size := len(arr)
	newSize := size - 1
	tmp := make([]string, newSize)
	copy(tmp, arr)
	j := 0
	for i := 0; i < size; i++ {
		if i == removeIndex {
			continue
		}
		tmp[j] = arr[i]
		j++
	}

	return tmp
}
func ArrayPrintln(values ...string) {
	tmp := "["
	for _, s := range values {
		tmp += s + ","
	}
	if l := len(tmp); l > 1 {
		tmp = StrSubstringEnd(tmp, l-1)
	}
	tmp += "]"
	fmt.Println(tmp)
}
