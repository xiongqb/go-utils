package common

import (
	"regexp"
)

// RegexGet 获取文本匹配的值（第一个值为文本本身，第二个值为第一个括号的匹配值，以此类推）
func RegexFind(text, regex string) string {
	compile := regexp.MustCompile(regex)
	return compile.FindString(text)
}

// RegexGet 获取文本匹配的值（第一个值为文本本身，第二个值为第一个括号的匹配值，以此类推）
func RegexGet(text, regex string) ([]string, error) {
	compile, err := regexp.Compile(regex)
	return compile.FindStringSubmatch(text), err
}

// RegexGetFirstMatch 获取第一个匹配值（不包含文本本身）
func RegexGetFirstMatch(text, regex string) (string, error) {
	arrays, err := RegexGet(regex, text)
	length := len(arrays)
	if length >= 2 {
		return arrays[1], nil
	} else {
		return "", err
	}
}

// RegexIsMatch 是否与文本匹配
func RegexIsMatch(text, regex string) (bool, error) {
	compile, err := regexp.Compile(regex)
	return len(compile.FindAllString(text, -1)) > 0, err
}

// RegexReplace 替换正则表达式中的部分
//
// RegexReplace("-w -s", `\s`, "@") => -w@-s，将一个空格替换为@
//
// RegexReplace("hi，$张三$! $李四$ 你们好。", `\$([^$]+)\$`, "<$1>") => hi，<张三>! <李四> 你们好。
//
// 匹配@开头结尾的如：`\@([^@]+)\@`。<$1>其中的$1代表第一个捕获组的内容
func RegexReplace(str, regex, replace string) string {
	re := regexp.MustCompile(regex)
	// 替换匹配到的部分
	return re.ReplaceAllString(str, replace)
}

// 是否包含
//
// RegexContains(`\s\s+`, "-w -s") => false 没有连续两个空格
func RegexContains(str, regex string) bool {
	re := regexp.MustCompile(regex)
	return re.MatchString(str)
}
