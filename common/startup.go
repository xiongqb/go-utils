package common

import (
	"fmt"
	"log"
	"os"
	"os/user"
	"path/filepath"
	"runtime"
	"strings"
)

/*
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>

		<key>Label</key>
		<string>${name}</string>
		<key>ProgramArguments</key>
	        <array>
	        	<string>/Applications/${name}.app/Contents/MacOS/${name}</string>
	        </array>
		<key>RunAtLoad</key>
		<true/>
		<key>WorkingDirectory</key>
		<string>/Applications/${name}.app/Contents/MacOS</string>
		<key>StandardErrorPath</key>
		<string>/tmp/${name}.err</string>
		<key>StandardOutPath</key>
		<string>/tmp/${name}.out</string>

</dict>
</plist>
*/
const (
	_MacPListContent = `
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>${name}</string>
	<key>ProgramArguments</key>
        <array>
			<!-- <string>/Applications/${name}.app/Contents/MacOS/${name}</string> -->
        	<string>${exePath}</string>
        </array>
	<key>RunAtLoad</key>
	<true/>
	<key>WorkingDirectory</key>
	<!-- <string>/Applications/${name}.app/Contents/MacOS</string> -->
	<string>${exeDirPath}</string>
	<key>StandardErrorPath</key>
	<string>~/.${name}/logs/startupAuto.err</string>
	<key>StandardOutPath</key>
	<string>~/.${name}/logs/startupAuto.out</string>
</dict>
</plist>
`

	_MacFilePList = `%s/Library/LaunchAgents/%s.plist`
	_WinFileBat   = `%s\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\startup-%s.bat`

	_WinBatContent = `@echo off
chcp 65001
IF EXIST %s (
    %s
)
`
	// _winNotifyTemplate = `PowerShell.exe -WindowStyle Hidden -Command "& {Add-Type -AssemblyName System.Windows.Forms; Add-Type -AssemblyName System.Drawing; $notify = New-Object System.Windows.Forms.NotifyIcon; $notify.Icon = [System.Drawing.SystemIcons]::%s; $notify.Visible = $true; $notify.ShowBalloonTip(1000000000, '%s', '%s', [System.Windows.Forms.ToolTipIcon]::None)}"`
)

// rem msg * /TIME:25 "【桌面小工具】检测到文件位置发送变化，需要重新手动解决开机启动"
// rem powershell -command "& {Add-Type -AssemblyName System.Windows.Forms; Add-Type -AssemblyName System.Drawing; $notify = New-Object System.Windows.Forms.NotifyIcon; $notify.Icon = [System.Drawing.SystemIcons]::Information; $notify.Visible = $true; $notify.ShowBalloonTip(1000000000, '123', '3456', [System.Windows.Forms.ToolTipIcon]::None)}"
// PowerShell.exe -WindowStyle Hidden -Command "[reflection.assembly]::loadwithpartialname('System.Windows.Forms');[reflection.assembly]::loadwithpartialname('System.Drawing');$notify = new-object system.windows.forms.notifyicon;$notify.icon = [System.Drawing.SystemIcons]::information;$notify.visible = $true;$notify.showballoontip(10000,'【桌面小工具】','检测到文件位置发送变化，需要重新勾选开机启动项！',[system.windows.forms.tooltipicon]::None)"

// 创建快捷方式
// https://github.com/jxeng/shortcut/blob/master/shortcut.go
// https://github.com/nyaosorg/go-windows-shortcut

// StartUpBySelf 自启动
// 实现原理是在LaunchAgents目录下写入启动配置文件配置说明。
// win则是在用户启动目录下写入一个bat来启动当前二进制。
// https://p00q.cn/?p=707
// https://www.fythonfang.com/blog/2021/4/19/mac-launchd-daemons-and-agents-tutorial
// C:\Users\user\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
func StartUpBySelf(on bool, name string) {
	var path, content string
	current, err := user.Current()
	if err != nil {
		ThrowException(err)
	}
	exePath, _ := filepath.Abs(os.Args[0])
	switch runtime.GOOS {
	case "darwin":
		path = fmt.Sprintf(_MacFilePList, current.HomeDir, name)
		content = strings.Replace(_MacPListContent, "${name}", name, -1)
		content = strings.Replace(content, "${exePath}", exePath, -1)
		content = strings.Replace(content, "${exeDirPath}", FileGetParentDir(exePath), -1)
	//case "linux":
	case "windows":
		path = fmt.Sprintf(_WinFileBat, current.HomeDir, name)

		//预防特殊目录名，如：C:\Program Files
		content = CmdGenerateBecomeSilentString(fmt.Sprintf(`'%s'`, exePath), "")
		// bat if EXIST 路径处理只能用双引号。powershell.exe -Command 要双引号转义\"，要么单引号'
		content = fmt.Sprintf(_WinBatContent, fmt.Sprintf(`"%s"`, exePath), content)
	default:
		log.Panicln("不支持的系统")
	}
	fmt.Println(path, "=>", content)
	log.Println(path, "=>", content)
	if on {
		FileWrite(path, content)
	} else {
		FileDelete(path)
	}
}

// IsStartUp 根据文件存在判断是否自启动
func IsStartUp(name string) bool {
	current, err := user.Current()
	if err != nil {
		ThrowException(err)
	}
	switch runtime.GOOS {

	case "darwin":
		return FileExists(fmt.Sprintf(_MacFilePList, current.HomeDir, name))
	case "windows":
		return FileExists(fmt.Sprintf(_WinFileBat, current.HomeDir, name))
	default:
		log.Println("IsStartUp 不支持的系统")
		return false
	}
}
