package common

import (
	"fmt"
	"strconv"
)

// 将任意对象字符化
func ToString(obj any) string {
	//str := "%v"
	//objType := InstanceOf(obj)
	//switch objType {
	//	case "int":
	//	case "int8":
	//	case "int16":
	//	case "int32":
	//	case "int64": str = "%d"; break
	//	case "float32":
	//	case "float64": str = "%f"; break
	//	case "string": str = "%s"; break
	//
	//}
	//对象类型转换实现类
	//switch obj.(type) {
	//case *KFC:
	//	kfc := c.(*KFC)
	//	kfc.Make....()
	//}

	return fmt.Sprintf("%v", obj) // see fmt
}

// int8：带符号的8位整数，取值范围从 -128 到 127。
func ByteParse(v string) int8 {
	i, err := strconv.ParseInt(v, 10, 8)
	if err != nil {
		ThrowException("ParserInt8(Byte) error:", err)
	}
	return int8(i)
}

// int16：带符号的16位整数，取值范围从 -32,768 到 32,767。
func ShortParse(v string) int16 {
	i, err := strconv.ParseInt(v, 10, 16)
	if err != nil {
		ThrowException("ParserInt16(Short) error:", err)
	}
	return int16(i)
}

// int32：带符号的32位整数，取值范围从 -2,147,483,648 到 2,147,483,647。(由于int在32位系统上通常是32位，在64位系统上通常是64位)
func IntParse(v string) int {
	i, err := strconv.ParseInt(v, 10, 0)
	if err != nil {
		ThrowException("ParserInt32(Int) error:", err)
	}
	return int(i)
}

// int64：带符号的64位整数，取值范围从 -9,223,372,036,854,775,808 到 9,223,372,036,854,775,807。
func LongParse(v string) int64 {
	i, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		ThrowException("ParseInt64(Long) error:", err)
	}
	return i
}

// 转浮点数
func FloatParse(v string) float64 {
	i, err := strconv.ParseFloat(v, 64)
	if err != nil {
		ThrowException("ParseFloat64 error:", err)
	}
	return i
}
