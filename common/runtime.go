package common

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

// ScheduledTask 定时任务
func ScheduledTask(time_ time.Time, taskName, taskPath, timeStr string) {
	format := time_.Format(TimeFormat)
	split := StrSplit(format, " ")
	dateStr := split[0]
	if len(timeStr) == 0 {
		timeStr = split[1]
	}
	ArrayPrintln(dateStr, timeStr)
	CmdRun("schtasks", "/create", "/f", "/tn", taskName, "/tr", taskPath, "/sc", "DAILY", "/sd", dateStr, "/st", timeStr)
}

// ScheduledTaskDelete 删除定时任务
func ScheduledTaskDelete(taskName string) {
	CmdRun("schtasks", "/delete", "/f", "/tn", taskName)
}
func CmdOnlyRun(commandName string, params ...string) error {
	cmd := exec.Command(commandName, params...)
	err := cmd.Run()
	return err
}

// 执行命令并返回结果
//
// 参数 commandLine 一行可执行命令，如:PowerShell.exe xxx
//
// return 返回预期的值即为成功
func CmdRunLine(commandLine string) {
	split := StrSplit(commandLine, " ")
	size := len(split)
	if size == 1 {
		log.Println(CmdRun(split[0]))
	} else {
		cmdName := split[0]
		ArrayPrintln(ArrayRemoveArrayStr(split, 0)...)

		log.Println(CmdRun(cmdName, ArrayRemoveArrayStr(split, 0)...))
	}

}

// 执行命令并返回结果
//
// 参数 commandName 执行者，如:PowerShell.exe
//
// 参数 params 不定参数
//
// return 返回预期的值即为成功
//
// see http://www.ay1.cc/article/26215.html
func CmdRun(commandName string, params ...string) string {
	// 过滤空字符串参数
	var filteredArgs []string
	for _, arg := range params {
		if arg != "" {
			filteredArgs = append(filteredArgs, CmdWrapPathString(arg))
		}
	}
	cmd := exec.Command(CmdWrapPathString(commandName), filteredArgs...)
	log.Println("CmdRun", cmd.Args)
	var out bytes.Buffer
	cmd.Stdout = &out      // 标准输出
	cmd.Stderr = os.Stderr // 标准错误
	err := cmd.Start()
	if err != nil {
		log.Println("执行命令失败", err)
		return ""
	}
	err = cmd.Wait()
	if err != nil {
		log.Println("等待命令执行失败", err)
	}
	return out.String()
}
func CmdDirRun(dir string, commandName string, params ...string) string {
	// 过滤空字符串参数
	var filteredArgs []string
	for _, arg := range params {
		if arg != "" {
			filteredArgs = append(filteredArgs, CmdWrapPathString(arg))
		}
	}
	cmd := exec.Command(commandName, filteredArgs...)
	log.Println("CmdDirRun", "["+dir+"]", cmd.Args)
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = os.Stderr
	cmd.Dir = dir
	err := cmd.Start()
	if err != nil {
		ThrowException("CmdDirRun", err.Error())
		return ""
	}
	err = cmd.Wait()
	if err != nil {
		ThrowException("CmdDirRun", err.Error())
	}
	return out.String()
}

// CmdRunByBat 通过生成bat文件执行命令，然后删掉。（一些特殊系统命令无法直接调用）
func CmdRunByBat(commandLine string) string {
	//让cmd支持中文
	tmp := "chcp 65001\n\r" + commandLine
	fileBat := FileOf(FileGetCurrentDir(), ToString(time.Now().Unix()), ".bat")
	FileWrite(fileBat, tmp)
	run := CmdRun(fileBat)
	FileDelete(fileBat)
	return run
}

// 启动其他程序
//
// 参数:becomeSilent  	静默启动标识
//
// 参数:cmdArray  		程序及其命令，第一个取程序
func CmdStartApp(becomeSilent bool, cmdArray ...string) error {
	cmdArray = removeEmptyEntries(cmdArray)
	size := len(cmdArray)
	if size == 1 {
		if IsWindows {
			return StartWinApp(becomeSilent, cmdArray[0], "")
		} else if IsMac {
			return StartMacApp(becomeSilent, cmdArray[0], "")
		}
		return fmt.Errorf("CmdStartApp %s", "该系统尚未实现")
	} else if size > 1 {
		appName := cmdArray[0]
		if IsWindows {
			return StartWinApp(becomeSilent, appName, strings.Join(ArrayRemoveArrayStr(cmdArray, 0), " "))
		} else if IsMac {
			return StartMacApp(becomeSilent, appName, strings.Join(ArrayRemoveArrayStr(cmdArray, 0), " "))
		}
		return fmt.Errorf("CmdStartApp %s", "该系统尚未实现")
	} else {
		return fmt.Errorf("CmdStartApp %s", "cmdArray 不能为空")
	}
}

// 启动其他程序
//
// 参数:becomeSilent  	静默启动标识
//
// 参数:cmdArray  		程序及其命令，第一个取程序
func CmdStartAppLine(becomeSilent bool, cmd string) error {
	return CmdStartApp(becomeSilent, StrSplit(cmd, " ")...)
}

func StartMacApp(becomeSilent bool, fileApp string, arg string) error {
	var temArg []string
	// 静默启动参数
	if becomeSilent {
		temArg = append(temArg, "-g")
	}
	// 这里谁都不能加引号，虽然终端需要，但这里一加引号就出错
	// 目标程序
	if len(fileApp) > 0 {
		if StrEndsWith(fileApp, ".app") {
			temArg = append(temArg, "-a")
		}
		temArg = append(temArg, fileApp)
	}
	// 目标程序参数
	if len(arg) > 0 {
		temArg = append(temArg, arg)
	}
	fmt.Println(ArrayInsertArrayStr(temArg, "open"))
	cmd := exec.Command("open", temArg...)

	// Run()函数的主要作用是执行一个指定的外部命令（通常是系统命令或程序），并等待该命令执行完成。
	// 当命令能够正常启动，顺利完成输入输出流（stdin、stdout、stderr）的复制，
	// 且最终以零退出状态码退出时，Run()函数将返回nil表示没有发生错误。
	err := cmd.Run()
	if nil != err {
		log.Println("start macAPP error:", "open", temArg, err)
	} else {
		log.Println("start macAPP:", fileApp, arg)
	}
	return err
}

// 启动其他程序
//
// 参数:becomeSilent  	静默启动标识
//
// 参数:fileApp 		待启动程序路径+名称
//
// 参数:arg 			待启动程序传入参数
//
// retrun err!=nil就是成功，其他错误 参考testStartWinApp
//
// https://www.bbsmax.com/A/VGzlk4NYdb/
//
// https://blog.csdn.net/hejingdong123/article/details/101369709
func StartWinApp(becomeSilent bool, fileApp string, arg string) error {
	if strings.Contains(fileApp, " ") {
		fileApp = CmdWrapPathString(fileApp)
	}
	var temArg []string

	// 隐藏powershell窗口
	temArg = append(temArg, "-WindowStyle")
	temArg = append(temArg, "Hidden")

	// 启动目标程序
	temArg = append(temArg, "-Command")
	temArg = append(temArg, "Start-Process")
	temArg = append(temArg, fileApp)
	// 目标程序参数
	if len(arg) > 0 {
		if strings.Contains(arg, " ") {
			arg = CmdWrapPathString(arg)
		}
		temArg = append(temArg, "-ArgumentList")
		temArg = append(temArg, arg)
	}

	// 静默启动参数
	if becomeSilent {
		temArg = append(temArg, "-WindowStyle")
		temArg = append(temArg, "Hidden")

	}

	fmt.Println(ArrayInsertArrayStr(temArg, "PowerShell.exe"))
	cmd := exec.Command("PowerShell.exe", temArg...)

	// 启动时隐藏powershell窗口,没有这句会闪一下powershell窗口
	// cmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
	// Run()函数的主要作用是执行一个指定的外部命令（通常是系统命令或程序），并等待该命令执行完成。
	// 当命令能够正常启动，顺利完成输入输出流（stdin、stdout、stderr）的复制，
	// 且最终以零退出状态码退出时，Run()函数将返回nil表示没有发生错误。
	err := cmd.Run()
	if nil != err {
		log.Println("start exe error:", "PowerShell.exe", temArg)
	} else {
		log.Println("start exe:", fileApp, arg)
	}
	return err
}

// 生成静默启动的执行命令（带PowerShell.exe）
func CmdGenerateBecomeSilentString(fileApp string, arg string) string {
	//generate
	if strings.Contains(fileApp, " ") {
		fileApp = CmdWrapPathString(fileApp)
	}
	var temArg []string

	// 隐藏powershell窗口
	temArg = append(temArg, "-WindowStyle")
	temArg = append(temArg, "Hidden")

	// 启动目标程序
	temArg = append(temArg, "-Command")
	temArg = append(temArg, "Start-Process")
	temArg = append(temArg, fileApp)
	// 目标程序参数
	if len(arg) > 0 {
		if strings.Contains(arg, " ") {
			arg = CmdWrapPathString(arg)
		}
		temArg = append(temArg, "-ArgumentList")
		temArg = append(temArg, arg)
	}

	// 静默启动参数
	temArg = append(temArg, "-WindowStyle")
	temArg = append(temArg, "Hidden")

	str := "PowerShell.exe " + strings.Join(temArg, " ")
	fmt.Println(str)
	return str
}

// 目标程序或参数有空格或者?时，则需要用单引号包裹，如：arg=>'arg'
func CmdWrapPathString(arg string) string {
	//参数里有多个连续空格，则直接替换单个空格返回
	if RegexContains(arg, `\s\s+`) {
		// 替换多个空格为单个空格
		return RegexReplace(arg, `\s\s+`, " ")
	}
	if strings.Contains(arg, " ") {

		if !strings.Contains(arg, `"`) && !strings.Contains(arg, "'") {
			// arg = "\"" + arg + "\""
			arg = fmt.Sprintf(`"%s"`, arg)
			//cmd不支持单引号
			// arg = "'" + arg + "'"
		}
	}
	return arg
}

// 目标程序或参数有空格，则需要用单引号包裹，如：arg=>'arg'
func CmdWrapPathStrings(args ...string) []string {
	var newArgs = make([]string, len(args))
	for i, arg := range args {
		newArgs[i] = CmdWrapPathString(arg)
	}
	return newArgs
}
