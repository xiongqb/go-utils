package common

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
)

// ResponseJSON 是用来存储响应体的通用 JSON 结构体
// 打印返回的 JSON 响应
// fmt.Printf("Response JSON: %+v\n", *response)
type JSONObject map[string]interface{}
type JSONArray map[string][]interface{}

// 结构体 序列化为 JSON字符串
func JsonToString(object interface{}) string {
	jsonData, err := json.Marshal(object)
	if err != nil {
		log.Println("JsonToString Error:", err)
		return ""
	}
	return string(jsonData)
}

// JSON字符串 反序列化为 结构体
func JsonStrToJson(str string) JSONObject {
	return JsonBytesToJson([]byte(str))
}

// bytes 反序列化为 结构体
func JsonBytesToJson(bytes []byte) JSONObject {
	var jsonObject JSONObject
	err := json.Unmarshal(bytes, &jsonObject)
	if err != nil {
		log.Println("JsonToString Error:", err)
		return nil
	}

	return jsonObject
}

// 通过key取值，如：JsonGetValueByKey(response,"data","token")【注意：可能为nil】
func JsonGetValueByKeys(jsonObject JSONObject, keys ...string) interface{} {
	v, _ := getNestedValue(jsonObject, keys)
	return v
}

// 通过key取值，如：JsonGetValueByKey(response,"data.token")【注意：可能为nil】
func JsonGetValueByKey(jsonObject JSONObject, key string) interface{} {
	v, _ := getNestedValue(jsonObject, StrSplit(key, "."))
	return v
}

// func getValueFromNestedJSON(response JSONObject, keys ...string) []interface{} {
// 	results := make([]interface{}, len(keys))

//		var current interface{} = response
//		for i, key := range keys {
//			keysSlice := strings.Split(key, ".")
//			for _, k := range keysSlice {
//				if m, ok := current.(map[string]interface{}); ok {
//					current = m[k]
//					if current == nil {
//						// 当键不存在时，将结果设为空
//						results[i] = nil
//						break
//					}
//				} else {
//					// 当遇到非映射类型时，后续键无法处理，将结果设为空
//					results[i] = nil
//					break
//				}
//			}
//			results[i] = current
//		}
//		return results
//	}
func JsonGetValueToMap120(json JSONObject, keys ...string) (JSONObject, error) {
	result := make(JSONObject)

	processNextKey := func(current JSONObject, remainingKeys []string, keyPrefix string) {
		var i int = 0
		for ; i < len(remainingKeys); i++ {
			fullKey := keyPrefix + remainingKeys[i]
			fmt.Println(fullKey)
			nested, ok := current[remainingKeys[i]].(JSONObject)
			if !ok || i == len(remainingKeys)-1 { // 当前键不存在或者已经是最后一个键，结束本次循环
				break
			}
			current = nested
			keyPrefix += remainingKeys[i] + "."
		}

		// 提取当前层级及其之后的所有键值对
		for ; i < len(remainingKeys); i++ {
			fullKey := keyPrefix + remainingKeys[i]
			value, err := getNestedValue(json, strings.Split(fullKey, "."))
			if err != nil {
				continue // 如果某个键路径不存在，则跳过
			}
			result[fullKey] = value
		}
	}

	for _, keyPath := range keys {
		keysSlice := strings.Split(keyPath, ".")
		processNextKey(json, keysSlice, "")
	}

	return result, nil
}

// 将深度json根据keys扁平化，result, err := common.JsonGetValueToMap(jsonObj, "result.token", "result.authMenu.10.authed")
func JsonGetValueToMap(jsonObject JSONObject, keys ...string) (JSONObject, error) {
	result := make(JSONObject)
	for _, keyPath := range keys {
		keysSlice := strings.Split(keyPath, ".")
		value, err := getNestedValue(jsonObject, keysSlice)
		if err != nil {
			result = nil
			return nil, err //错了就不执行了
			// 	continue // 如果某个键路径不存在，则跳过
		}
		// 直接将结果写入扁平化的JSONObject
		flattenedKey := strings.Join(keysSlice, ".")
		result[flattenedKey] = value
	}

	return result, nil
}
func getNestedValue(jsonObject JSONObject, keys []string) (interface{}, error) {
	var value interface{} = jsonObject
	for i, key := range keys {
		//判断是否JSONObject类型
		if m, ok := value.(JSONObject); ok {
			value, ok = m[key]
			if !ok {
				return nil, fmt.Errorf("未找到对应的Key %s 请查证", strings.Join(keys[:i+1], "."))
			}
			//判断是否map类型
		} else if m, ok := value.(map[string]interface{}); ok {
			value, ok = m[key]
			if !ok {
				return nil, fmt.Errorf("未找到对应的Key %s 请查证", strings.Join(keys[:len(keys)-1], "."))
			}
		} else if m, ok := value.([]interface{}); ok {
			num, err := strconv.Atoi(key)
			if err != nil {
				return nil, fmt.Errorf("索引 %s 转换失败，此处为数组，请提供范围内数字 %d ", strings.Join(keys[:len(keys)-1], "."), len(m))
			}
			if num < len(m) {
				value = m[num]
			} else {
				return nil, fmt.Errorf("索引 %s 超出数组的长度 %d ", strings.Join(keys[:len(keys)-1], "."), len(m))
			}
		} else {
			return nil, fmt.Errorf("无法在非对象中导航以获取键 %s", strings.Join(keys[:len(keys)-1], "."))
		}
	}
	return value, nil
}
