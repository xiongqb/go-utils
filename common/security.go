package common

import (
	"crypto/md5"
	"crypto/sha1"
	"fmt"
	"hash/crc32"
	//"github.com/spaolacci/murmur3"
)

//func murmur32Hash() uint32 {
//	return murmur3.Sum32([]byte(str))
//}
//
//func murmur64Hash() uint64 {
//	return murmur3.Sum64([]byte(str))
//}

func MD5(str string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(str)))
}
func MD5_(bytes []byte) string {
	return fmt.Sprintf("%x", md5.Sum(bytes))
}
func MD5Hash(bytes []byte) [16]byte {
	return md5.Sum(bytes)
}
func SHA1(str string) string {
	return fmt.Sprintf("%x", sha1.Sum([]byte(str)))
}
func SHA1_(bytes []byte) string {
	return fmt.Sprintf("%x", sha1.Sum(bytes))
}
func SHA1Hash(bytes []byte) [20]byte {
	return sha1.Sum(bytes)
}
func CRC32(str string) int {
	ieee := crc32.ChecksumIEEE([]byte(str))
	return int(ieee)
}
func CRC32_(bytes []byte) int {
	ieee := crc32.ChecksumIEEE(bytes)
	return int(ieee)
}
func CRC32Hash(bytes []byte) uint32 {
	return crc32.ChecksumIEEE(bytes)
}
