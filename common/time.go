package common

import (
	"strings"
	"time"
)

const TimeFormat = "2006-01-02 15:04:05"

// const TIMEZONE *time.Location
var (
	// TIMEZONE, tErr = time.LoadLocation("Asia/Shanghai")一些可能没得这个时区

	// 创建一个新的时区（定义北京时间相对于 UTC 的偏移量）
	TIMEZONE = time.FixedZone("Asia/Shanghai", 8*60*60)
)

// TimeNow 可能涉及时区问题，强烈建议用此方法
func TimeNow() time.Time {
	return time.Now().In(TIMEZONE)
}
func TimePlusSecond(t time.Time, seconds int) time.Time {
	return t.Add(time.Second * time.Duration(seconds))
}
func TimePlusMinute(t time.Time, minutes int) time.Time {
	return t.Add(time.Minute * time.Duration(minutes))
}
func TimePlusHour(t time.Time, hours int) time.Time {
	return t.Add(time.Hour * time.Duration(hours))
}
func TimePlusDay(t time.Time, day int) time.Time {
	return TimePlusHour(t, 24*day)
}
func TimePlusWeek(t time.Time, week int) time.Time {
	return TimePlusDay(t, 7*week)
}
func TimePlusMonth(t time.Time, months int) time.Time {
	return t.AddDate(0, months, 0)
}
func TimePlusYear(t time.Time, years int) time.Time {
	return t.AddDate(years, 0, 0)
}
func TimePlus(t time.Time, years, months, days int) time.Time {
	return t.AddDate(years, months, days)
}

// TimeNowString 2006-01-02 15:04:05
func TimeNowString() string {
	return TimeNow().Format(TimeFormat)
}

// TimeToString 2006-01-02 15:04:05
func TimeToString(t time.Time) string {
	return t.In(TIMEZONE).Format(TimeFormat)
}

// TimeStringToTime 2006-01-02 15:04:05
func TimeStringToTime(t string) time.Time {
	//parse, err := time.Parse(TimeFormat, t)
	parse, err := time.ParseInLocation(TimeFormat, t, TIMEZONE)
	if err != nil {
		ThrowException("TimeStringToTime", err)
	}
	return parse
}
func TimeIntToTime(seconds int64) time.Time {
	return time.Unix(seconds, 0).In(TIMEZONE)
}

// TimeIntToTimeString 2006-01-02 15:04:05
func TimeIntToTimeString(seconds int64) string {
	return time.Unix(seconds, 0).In(TIMEZONE).Format(TimeFormat)
}

// TimeToStringFormat 可以使用yyyy-MM-dd HH:mm:ss
func TimeToStringFormat(t time.Time, format string) string {
	if len(format) == 0 {
		format = TimeFormat
	} else {
		//2006-01-02 15:04:05
		format = strings.Replace(format, "yyyy", "2006", -1)
		format = strings.Replace(format, "MM", "01", -1)
		format = strings.Replace(format, "dd", "02", -1)
		format = strings.Replace(format, "HH", "15", -1)
		format = strings.Replace(format, "mm", "04", -1)
		format = strings.Replace(format, "ss", "05", -1)
	}
	return t.In(TIMEZONE).Format(format)
}

// Sleep 1 second = 1000 millisecond
func TimeSleep(millisecond int) {
	duration, err := time.ParseDuration(ToString(millisecond) + "ms")
	if err != nil {
		ThrowException("Sleep", err)
	}
	time.Sleep(duration)
}
