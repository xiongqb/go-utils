package common

import "strings"

// Boolean 如果表示true或1的字符，都返回true，反之false
func Boolean(b string) bool {
	b = StrTrim(strings.ToLower(b))
	return b == "true" || b == "1"
}

// BooleanOf 仅表示true的字符，返回true，反之false
func BooleanOf(b string) bool {
	b = StrTrim(strings.ToLower(b))
	return b == "true"
}
