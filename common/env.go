package common

import "os"

func EnvIsDev() bool {
	return os.Getenv("GO_ENV") == "dev"
}
